# how to run the demo

- go to the folder where the code is "checkouted"

## build the project & create an "exploded" version of the distribution package
- mvn clean install -Ddistribution=development

## launch karaf with the demo
- .\distribution\target\fr.matoeil.demo.osgi.declarative.services.distribution-1.0.0-SNAPSHOT\bin\karaf clean debug

## test it
- you should see the log message "echo : echo" if provider & consumer component are correctly started

## re test it
- go to karaf shell
- type "list"
- get the id of "fr.matoeil.demo.osgi.declarative.services.provider" bundle
- type "stop {id}". replace {id} by the real id get from the previous step
- type "start {id}".
- you should see the log message "echo : echo" again

## re re test it
- open a web browser
- go to "http://localhost:8080/rest/Echo?message=echo"
- the page content is an "echo" of the query parameter message send to the rest endpoint
- go to karaf shell
- type "list"
- get the id of "fr.matoeil.demo.osgi.declarative.services.provider" bundle
- type "stop {id}". replace {id} by the real id get from the previous step
- go to "http://localhost:8080/rest/Echo?message=echo"
- you get a 404 error code
- type "start {id}".
- go to "http://localhost:8080/rest/Echo?message=echo"
- the page content is an "echo" of the query parameter message again

## voila

# under the hood

- In the "api" module, the package fr.matoeil.demo.osgi.declarative.services.api contains :
    - the EchoService interface representing the echo service

- In the "provider" module, the package fr.matoeil.demo.osgi.declarative.services.provider.internal contains :
    - the SimpleEchoService.java class. This class is annotated with the @Component annotation.
        With the help of the maven-scr-plugin (see pom.xml of the provider module), declarative service files will be generated.
        At the the runtime, the service component runtime (SCR) will parse these generated files and publish the SimpleEchoService in the OSGi service registry.

- In the "consumer" module, the package fr.matoeil.demo.osgi.declarative.services.consumer.internal contains :
    - the SimpleConsumer.java class. This class is annotated with the @Component annotation. It contains three others annotations.
        @Reference will tell the SCR that SimpleConsumer need a reference to an EchoService instance.
        @Activate mark the method to be call when the component is activated.
        @DeActivate mark the method to be call when the component is deactivated.
        According to the SCR lifecycle spec, by default, the SimpleConsumer will be started once the EchoService reference is resolved.

- The "rest" module is an experiment to mix declarative service & guice.

- In the "rest" module, the fr.matoeil.demo.osgi.declarative.services.rest.internal package contains :
    - the EchoResource.java is a JAX-RS resource that provide a rest service doing a echo according a message query parameter.
        It need a EchoService instance that will be provider by guice with the help of JSR-330 annotations
- In the "rest" module, the fr.matoeil.demo.osgi.declarative.services.rest.internal.activation package contains :
    - The Activator.java is responsible to get a reference to an EchoService and to register the rest servlet.
        As for the SimpleConsumer, it relies on the Declarative Service annotations.
        Thanks to the licycle of a Declarative Service component, once the method start is called, we have a static reference to the EchoService.
        According that, we can use standard guice mecanisms in order to inject this reference to the EchoResource.
        Guice inject a GuiceContainer (a sun Jersey stuff), representing the rest servlet, into the Activator and as a consequence
        we can register it into the Osgi service Registry.
        The pax-web service will be notified of this registration and will deploy the servlet into the underlying jetty container.
        And voila, the SimpleEchoService is available through a web browser.
    - The BundleModule.java is a guice module defining the internal wiring of the objects of the bundle.
        It get the EchoService reference by its constructor and configure all the sun jersey (JAX-RS) related stuff.
    - As a nice side effect, if the provider bundle is desactivated, the rest servlet will be deactivated too.
        It means for the developer he don't need to be concerned by the dynamism of OSGi.

- The "features" module contains a features.xml files describing the different osgi bundle to deploy in the osgi (karaf) runtime

- The "distribution" module contains files describing how to generate the distribution archive. Two profiles are supported:
    - one generate a tar.gz and zip files. Only decompress a distribution archive, launch the script "./bin/karaf" in order to launch the demo.
        Use the maven command "mvn clean install -Ddistribution=release" to generate it.
    - one generate a exploded version of the distribution archive into the folder "distribution\target\fr.matoeil.demo.osgi.declarative.services.distribution-1.0.0-SNAPSHOT".
        Only launch the script "./bin/karaf" in this folder in order to launch the demo
        Use the maven command "mvn clean install -Ddistribution=development" to generate it.



