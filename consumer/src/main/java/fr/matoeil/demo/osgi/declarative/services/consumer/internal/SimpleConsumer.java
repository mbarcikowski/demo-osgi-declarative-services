/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.demo.osgi.declarative.services.consumer.internal;

import fr.matoeil.demo.osgi.declarative.services.api.EchoService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author mathieu.barcikowski@gmail.com
 */
@Component
public class SimpleConsumer {
    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleConsumer.class);

    private EchoService echoService_;


    @Activate
    public void start() {
        LOGGER.debug("SimpleConsumer.start ");
        LOGGER.debug("echo : {}", echoService_.doEcho("echo"));
    }


    @Deactivate
    public void stop() {
        LOGGER.debug("SimpleConsumer.stop");
    }

    @Reference
    public void bindEchoService(final EchoService aEchoService) {
        LOGGER.debug("SimpleConsumer.bindEchoService");
        echoService_ = aEchoService;
    }

    public void unbindEchoService(final EchoService aEchoService) {
        LOGGER.debug("SimpleConsumer.unbindEchoService");
        echoService_ = null;
    }
}
