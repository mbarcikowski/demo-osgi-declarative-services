<?xml version="1.0" encoding="UTF-8"?>

<!--
  ~ Copyright 2012-2013 Mathieu Barcikowski
  ~
  ~  Licensed under the Apache License, Version 2.0 (the "License");
  ~  you may not use this file except in compliance with the License.
  ~  You may obtain a copy of the License at
  ~
  ~      http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  -->

<features xmlns="http://karaf.apache.org/xmlns/features/v1.0.0" name="demo-osgi-declarative-services-${project.version}">
    <feature name="demo-osgi-declarative-services" version="${project.version}">
        <feature version="${project.version}">demo-osgi-declarative-services-dependencies</feature>
        <bundle start-level="80" start="true">mvn:fr.matoeil.demo.osgi.declarative.services/fr.matoeil.demo.osgi.declarative.services.api/${modules.fr.matoeil.demo.osgi.declarative.services.api.version}</bundle>
        <bundle start-level="80" start="true">mvn:fr.matoeil.demo.osgi.declarative.services/fr.matoeil.demo.osgi.declarative.services.provider/${modules.fr.matoeil.demo.osgi.declarative.services.provider.version}</bundle>
        <bundle start-level="80" start="true">mvn:fr.matoeil.demo.osgi.declarative.services/fr.matoeil.demo.osgi.declarative.services.consumer/${modules.fr.matoeil.demo.osgi.declarative.services.consumer.version}</bundle>
        <bundle start-level="80" start="true">mvn:fr.matoeil.demo.osgi.declarative.services/fr.matoeil.demo.osgi.declarative.services.rest/${modules.fr.matoeil.demo.osgi.declarative.services.rest.version}</bundle>
    </feature>

    <feature name="demo-osgi-declarative-services-dependencies" version="${project.version}">
        <!-- osgi -->
        <bundle start-level="30" dependency="true"
                start="true">mvn:org.osgi/org.osgi.compendium/${dependencies.org.osgi.compendium.version}
        </bundle>

        <bundle start-level="30" dependency="true" start="true">mvn:com.google.guava/guava/${dependencies.guava.version}</bundle>

        <!-- dependency injection -->
        <bundle start-level="30" dependency="true"
                start="true">mvn:javax.inject/com.springsource.javax.inject/${dependencies.com.springsource.javax.inject.version}
        </bundle>


        <!-- guice -->
        <bundle start-level="30" dependency="true"
                start="true">mvn:org.aopalliance/com.springsource.org.aopalliance/${dependencies.com.springsource.org.aopalliance.version}
        </bundle>
        <bundle start-level="30" dependency="true"
                start="true">mvn:com.google.inject/guice/${dependencies.guice.version}
        </bundle>
        <bundle start-level="30" dependency="true" start="true">mvn:com.google.inject.extensions/guice-servlet/${dependencies.guice.version}
        </bundle>
        <bundle start-level="30" dependency="true"
                start="true">mvn:org.ops4j/peaberry/${dependencies.peaberry.version}
        </bundle>

        <!-- netty -->
        <bundle start-level="30" dependency="true" start="true">mvn:io.netty/netty/${dependencies.netty.version}
        </bundle>

        <!-- jersey -->
        <bundle start-level="30" dependency="true" start="true">mvn:com.sun.jersey/jersey-servlet/${dependencies.jersey.version}</bundle>
        <bundle start-level="30" dependency="true" start="true">mvn:com.sun.jersey/jersey-server/${dependencies.jersey.version}</bundle>
        <bundle start-level="30" dependency="true" start="true">mvn:com.sun.jersey/jersey-core/${dependencies.jersey.version}</bundle>
        <bundle start-level="30" dependency="true" start="true">mvn:com.sun.jersey.contribs/jersey-guice/${dependencies.jersey.version}</bundle>
        <bundle start-level="30" dependency="true" start="true">mvn:org.codehaus.jettison/jettison/${dependencies.jettison.version}</bundle>

        <!-- jackson -->
        <bundle start-level="30" dependency="true" start="true">mvn:org.codehaus.jackson/jackson-core-asl/${dependencies.jackson.version}
        </bundle>
        <bundle start-level="30" dependency="true" start="true">mvn:org.codehaus.jackson/jackson-mapper-asl/${dependencies.jackson.version}
        </bundle>
        <bundle start-level="30" dependency="true" start="true">mvn:org.codehaus.jackson/jackson-jaxrs/${dependencies.jackson.version}</bundle>

        <!-- web -->
        <bundle start-level="30" dependency="true"
                start="true">mvn:javax.servlet/javax.servlet-api/${dependencies.javax.servlet.api.version}
        </bundle>
        <bundle dependency="true"
                start-level="30">mvn:org.apache.geronimo.specs/geronimo-activation_1.1_spec/${dependencies.geronimo.activation.1.1.spec.version}
        </bundle>
        <bundle dependency="true"
                start-level="30">mvn:org.apache.geronimo.specs/geronimo-servlet_3.0_spec/${dependencies.geronimo.servlet.3.0.spec.version}
        </bundle>
        <bundle dependency="true" start-level="30">mvn:javax.mail/mail/${dependencies.mail.version}</bundle>
        <bundle dependency="true"
                start-level="30">mvn:org.apache.geronimo.specs/geronimo-jta_1.1_spec/${dependencies.geronimo.jta.1.1.spec.version}
        </bundle>
        <bundle dependency="true"
                start-level="30">mvn:org.apache.geronimo.specs/geronimo-annotation_1.1_spec/${dependencies.geronimo.annotation.1.1.spec.version}
        </bundle>
        <bundle dependency="true"
                start-level="30">mvn:org.apache.geronimo.specs/geronimo-jaspic_1.0_spec/${dependencies.geronimo.jaspic.1.0.spec.version}
        </bundle>
        <bundle start-level="30" dependency="true">mvn:org.eclipse.jetty.aggregate/jetty-all-server/${dependencies.jetty.version}</bundle>
        <config name="org.ops4j.pax.web">javax.servlet.context.tempdir=${karaf.data}/pax-web-jsp</config>
        <bundle start-level="30" dependency="true">mvn:org.ops4j.pax.web/pax-web-api/${dependencies.pax.web.version}</bundle>
        <bundle start-level="30" dependency="true">mvn:org.ops4j.pax.web/pax-web-spi/${dependencies.pax.web.version}</bundle>
        <bundle start-level="30" dependency="true">mvn:org.ops4j.pax.web/pax-web-runtime/${dependencies.pax.web.version}</bundle>
        <bundle start-level="30" dependency="true">mvn:org.ops4j.pax.web/pax-web-jetty/${dependencies.pax.web.version}</bundle>
        <bundle start-level="30" dependency="true">mvn:org.ops4j.pax.web/pax-web-jsp/${dependencies.pax.web.version}</bundle>
        <config name="org.ops4j.pax.url.war">org.ops4j.pax.url.war.importPaxLoggingPackages=true</config>
        <bundle start-level="30" dependency="true">mvn:org.ops4j.pax.web/pax-web-extender-war/${dependencies.pax.web.version}</bundle>
        <bundle start-level="30" dependency="true">mvn:org.ops4j.pax.web/pax-web-extender-whiteboard/${dependencies.pax.web.version}</bundle>
        <bundle start-level="30" dependency="true">mvn:org.ops4j.pax.web/pax-web-deployer/${dependencies.pax.web.version}</bundle>
        <bundle start-level="30" dependency="true">mvn:org.ops4j.pax.url/pax-url-war/${dependencies.pax.url.version}</bundle>
        <bundle start-level="30" dependency="true">mvn:org.ops4j.pax.url/pax-url-commons/${dependencies.pax.url.version}</bundle>
        <bundle start-level="30" dependency="true">mvn:org.ops4j.pax.swissbox/pax-swissbox-bnd/${dependencies.pax.swissbox.version}</bundle>
        <bundle start-level="30" dependency="true">mvn:org.ops4j.pax.swissbox/pax-swissbox-property/${dependencies.pax.swissbox.version}</bundle>
        <bundle start-level="30" dependency="true">mvn:org.ops4j.base/ops4j-base-net/${dependencies.ops4j.base.version}</bundle>
        <bundle start-level="30" dependency="true">mvn:org.ops4j.base/ops4j-base-lang/${dependencies.ops4j.base.version}</bundle>
        <bundle start-level="30" dependency="true">mvn:org.ops4j.base/ops4j-base-monitors/${dependencies.ops4j.base.version}</bundle>
        <bundle start-level="30" dependency="true">mvn:org.ops4j.base/ops4j-base-util-property/${dependencies.ops4j.base.version}</bundle>
        <bundle start-level="30" dependency="true">mvn:biz.aQute/bndlib/${dependencies.bndlib.version}</bundle>


        <bundle start-level="30" dependency="true">mvn:org.apache.felix/org.apache.felix.metatype/${dependencies.felix.metatype.version}</bundle>
        <!--<bundle start-level="30" dependency="true">mvn:org.apache.karaf.webconsole/org.apache.karaf.webconsole.branding/${dependencies.apache.karaf.version}</bundle>
        <bundle start-level="30" dependency="true">mvn:org.apache.karaf.webconsole/org.apache.karaf.webconsole.console/${dependencies.apache.karaf.version}</bundle>
        <bundle start-level="30" dependency="true">mvn:org.apache.karaf.webconsole/org.apache.karaf.webconsole.admin/${dependencies.apache.karaf.version}</bundle>
        <bundle start-level="30" dependency="true">mvn:org.apache.karaf.webconsole/org.apache.karaf.webconsole.features/${dependencies.apache.karaf.version}</bundle>
        <bundle start-level="30" dependency="true">mvn:org.apache.karaf.webconsole/org.apache.karaf.webconsole.gogo/${dependencies.apache.karaf.version}</bundle>-->
        <bundle start-level="30" dependency="true">mvn:org.apache.felix/org.apache.felix.webconsole.plugins.event/${dependencies.felix.webconsole.plugins.event.version}</bundle>


        <bundle start-level="30" dependency="true">mvn:de.twentyeleven.skysail/org.json-osgi/${dependencies.org.json.osgi.version}</bundle>
        <bundle start-level="30" dependency="true">mvn:org.apache.felix/org.osgi.service.obr/${dependencies.org.osgi.service.obr.version}</bundle>
        <bundle start-level="30" dependency="true">mvn:org.apache.felix/org.apache.felix.scr/${dependencies.felix.scr.version}</bundle>
        <bundle start-level="30" dependency="true">mvn:org.apache.felix/org.apache.felix.webconsole/${dependencies.felix.webconsole.version}</bundle>
        <bundle start-level="30" dependency="true">mvn:org.apache.felix/org.apache.felix.webconsole.plugins.ds/${dependencies.felix.webconsole.plugins.ds.version}</bundle>
        <bundle start-level="30" dependency="true">mvn:org.apache.felix/org.apache.felix.webconsole.plugins.upnp/${dependencies.felix.webconsole.plugins.upnp.version}</bundle>
        <bundle start-level="30" dependency="true">mvn:org.apache.felix/org.apache.felix.webconsole.plugins.packageadmin/${dependencies.felix.webconsole.plugins.packageadmin.version}</bundle>
        <bundle start-level="30" dependency="true">mvn:org.apache.felix/org.apache.felix.webconsole.plugins.obr/${dependencies.felix.webconsole.plugins.obr.version}</bundle>
        <bundle start-level="30" dependency="true">mvn:org.apache.felix/org.apache.felix.webconsole.plugins.memoryusage/${dependencies.felix.webconsole.plugins.memoryusage.version}</bundle>
        <bundle start-level="30" dependency="true">mvn:org.apache.geronimo.specs/geronimo-servlet_2.5_spec/${dependencies.geronimo.servlet.2.5.spec.version}</bundle>
    </feature>
</features>

